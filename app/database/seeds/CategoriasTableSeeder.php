<?php

class CategoriasTableSeeder extends Seeder {

	public function run()
	{
		DB::table('categorias')->delete();

		Categoria::create(array(
			'titulo' => 'Games'
		));
	}

}