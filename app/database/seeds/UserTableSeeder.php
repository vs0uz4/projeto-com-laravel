<?php

class UserTableSeeder extends Seeder {

	public function run()
	{
		DB::table('users')->delete();

		User::create(array(
			'email' => 'samuelneivacarvalho@gmail.com',
			'password' => Hash::make('asbr1515')
		));
        
        User::create(array(
			'email' => 'teste@teste.com.br',
			'password' => Hash::make('123456')
		));
	}

}