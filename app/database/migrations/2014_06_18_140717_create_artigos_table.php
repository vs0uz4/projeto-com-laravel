<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArtigosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('artigos', function($table){
			$table->increments('id');
			$table->string('titulo');
			$table->text('conteudo');
			$table->string('img');
			$table->timestamps();
		});


		Schema::table('artigos', function($table) {
       		$table->integer('categorias_id')->unsigned()->nullable();
       
       		$table->foreign('categorias_id')->references('id')->on('categorias')->onDelete('cascade')->onUpdate('cascade');
   		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('artigos', function($table){
			$table->dropForeign('categorias_id');
			$table->dropColumn('categorias_id');
		});
   
		Schema::dropIfExists('listas'); //se a tabela existir, exclui
	}

}
