<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', array('as' => 'home', function () {
	return View::make('home');
}));


Route::group( array('before' => 'auth'), function () {
	/* LOGOUT */
    Route::get('administrator/logout', array('as' => 'logout', 'uses' => 'AdministratorController@logoutAdmin'));
	
	
	
	/* CATEGORIAS - ADMIN */
	//lista categoria
	Route::get('administrator/categorias', 'CategoriasController@index');

	//add categoria
	Route::get('administrator/categorias/adicionar', 'CategoriasController@adicionarCategoria');

	Route::post('administrator/categorias/adicionar', 'CategoriasController@adicionarCategoriaSalvar');
    
    //edit categoria
    Route::get('administrator/categorias/{id}', 'CategoriasController@editarCategoria');
    
    Route::post('administrator/categorias/{id}', 'CategoriasController@atualizarCategoria');

    //delete categoria
    Route::get('administrator/categorias/excluir/{id}', 'CategoriasController@excluirCategoria');
	
	

	/* ARTIGOS - ADMIN */
	Route::get('administrator/artigos', array('as' => 'Artigos', 'uses' => 'ArtigosController@index'));

	//add categoria
	Route::get('administrator/artigos/adicionar', array('as' => 'Artigos', 'uses' => 'ArtigosController@adicionarArtigo'));

	Route::post('administrator/artigos/adicionar', array('as' => 'Artigos', 'uses' => 'ArtigosController@adicionarArtigoSalvar'));


});

Route::group(array('before' => 'guest'), function(){

	/* ADMIN */
	Route::get('administrator', array('as' => 'administrator', 'uses' => 'AdministratorController@getAdmin'));
	
	Route::post('administrator', 'AdministratorController@postAdmin');	
});



