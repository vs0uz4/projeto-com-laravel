<!DOCTYPE html>

<html lang="pt-BR">
<head>
	<meta charset="UTF-8">
	<title>Projeto Laravel</title>
	{{ HTML::style("assets/css/style.css") }}
	{{ HTML::style("assets/css/bootstrap.css") }}
	{{ HTML::style("assets/css/bootstrap-responsive.min.css") }}
	{{ HTML::script("assets/js/jquery-1.11.1.min.js", array('type' => 'text/javascript' )) }}
	{{ HTML::script("assets/js/bootstrap.js", array('type' => 'text/javascript' )) }}
	{{ HTML::script("assets/js/functions.js", array('type' => 'text/javascript' )) }}	
</head>
<body>

	
	<div id="topo">
		<div class="centro-topo">
			<ul class="nav nav-pills">
				<li class="padding-top"><a href="">Home</a></li>
				<li class="padding-top"><a href="">Sobre</a></li>
				<li><a href="/">{{ HTML::image('assets/img/logo.png', 'Projeto Laravel', array('id' => 'PL-logo')) }}</a></li>
				<li class="padding-top"><a href="">Categotias</a></li>
				<li class="padding-top"><a href="">Contato</a></li>
		</div>

	</div>
	<div id="content">
		<div class="centro-content">
			<div class="padding-centro">
				<div class="conteudo">
					<div class="padding-conteudo">
						@section('conteudo')
						@show
					</div>
				</div>
				<div class="sidebar">
					<div class="box-pesquisa">
						{{ Form::open(array('url' => 'foo/bar')) }}
    						{{ Form::text('search', 'Faça sua pesquisa...') }}
						{{ Form::close() }}
					</div>
					
					<div class="categorias">
						<div class="padding-categorias">
							<div class="titulo">
								<p><a href="#">Categorías</a></p>
							</div>
							<ul class="lista-principal">
								<li><a href="#">Web Design</a></li>
								<li><a href="#">Programação Web</a></li>
								<li><a href="#">Java</a></li>
								<li><a href="#">C++</a></li>
								<li><a href="#">C#</a></li>
								<li><a href="#">CSS</a></li>
							</ul>
							<ul>
								<li><a href="#">Arduino</a></li>
								<li><a href="#">Linux</a></li>
								<li><a href="#">Laravel</a></li>
								<li><a href="#">CodeIgniter</a></li>
								<li><a href="#">Android</a></li>
								<li><a href="#">Iphone</a></li>
							</ul>
						</div>
					</div>

					<div class="autor">
						<div class="padding-autor">
							<div class="titulo">
								<p><a href="">Sobre o autor</a></p>
							</div>
							<div class="img-autor">
								<a href="#">{{ HTML::image('assets/img/users/1526999_554343727992521_1385920241_n.jpg', 'Samuel Neiva Carvalho', array('id' => 'UserPhoto', 'width' => '130px')) }}</a>
							</div>
							<div class="txt-autor">
								<p>Ser programador ou nao ser. Eis a questão.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="clear"></div>
	<div id="footer">
		<div class="centro-footer">
			<p>© Samuel Carvalho. Todos os direitos reservados.</p>
		</div>
	</div>

</body>
</html>
