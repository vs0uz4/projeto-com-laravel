@extends('template')

@section('conteudo')
	<div class="intro-text">
		<p>Olá e bem vindos! Meu nome é Samuel Carvalho, eu sou um programador. Neste blog estou aprendendo a usar o Laravel 4</p>
	</div>
	<div class="content">
		
		<div class="noticias">
			<div class="float-left-noticias">
				<div class="data-noticias">
					<p>10 de junho, 2014</p>
				</div>
			</div>
			<div class="float-right-noticias">
				<div class="titulo-noticias">
					<p><a href="#">Titulo da primeira noticia</a></p>
				</div>
				<div class="intro-text-noticias">
					<p>Vestibulum ante in faucibus orci luctus et ultrices posuere cubilia Curae; Proin vel ante a orci tempus eleifend ut et magna. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus luctus urna sed urna ultricies ac tempor dui sagittis. In condimentum facilisis porta. Sed nec diam eu diam mattis viverra. Nulla fringilla, orci ac euismod semper, magna diam porttitor mauris, quis sollicitudin. <a href="#" class="btn-link">Leia Mais...</a></p>
				</div>
				<div class="rodape-noticias">
					<div class="author">
						<span class="icon-user"></span>
						<p>Escrito por <a href="#" class="btn-link">Samuel Carvalho</a> em <a href="#" class="btn-link">Web Design</a> com <a href="#" class="btn-link">3 comentários</a></p>
					</div>
				</div>
			</div>
		</div>

		<div class="noticias">
			<div class="float-left-noticias">
				<div class="data-noticias">
					<p>10 de junho, 2014</p>
				</div>
			</div>
			<div class="float-right-noticias">
				<div class="titulo-noticias">
					<p><a href="#">Titulo da primeira noticia</a></p>
				</div>
				<div class="intro-text-noticias">
					<p>Vestibulum ante in faucibus orci luctus et ultrices posuere cubilia Curae; Proin vel ante a orci tempus eleifend ut et magna. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus luctus urna sed urna ultricies ac tempor dui sagittis. In condimentum facilisis porta. Sed nec diam eu diam mattis viverra. Nulla fringilla, orci ac euismod semper, magna diam porttitor mauris, quis sollicitudin. <a href="#" class="btn-link">Leia Mais...</a></p>
				</div>
				<div class="rodape-noticias">
					<div class="author">
						<span class="icon-user"></span>
						<p>Escrito por <a href="#" class="btn-link">Samuel Carvalho</a> em <a href="#" class="btn-link">Web Design</a> com <a href="#" class="btn-link">3 comentários</a></p>
					</div>
				</div>
			</div>
		</div>

		<div class="noticias">
			<div class="float-left-noticias">
				<div class="data-noticias">
					<p>10 de junho, 2014</p>
				</div>
			</div>
			<div class="float-right-noticias">
				<div class="titulo-noticias">
					<p><a href="#">Titulo da primeira noticia</a></p>
				</div>
				<div class="intro-text-noticias">
					<p>Vestibulum ante in faucibus orci luctus et ultrices posuere cubilia Curae; Proin vel ante a orci tempus eleifend ut et magna. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus luctus urna sed urna ultricies ac tempor dui sagittis. In condimentum facilisis porta. Sed nec diam eu diam mattis viverra. Nulla fringilla, orci ac euismod semper, magna diam porttitor mauris, quis sollicitudin. <a href="#" class="btn-link">Leia Mais...</a></p>
				</div>
				<div class="rodape-noticias">
					<div class="author">
						<span class="icon-user"></span>
						<p>Escrito por <a href="#" class="btn-link">Samuel Carvalho</a> em <a href="#" class="btn-link">Web Design</a> com <a href="#" class="btn-link">3 comentários</a></p>
					</div>
				</div>
			</div>
		</div>
		<div class="pagination-box">
			<div class="pagination-txt">
				<p>Pagina 1 de 57</p>
			</div>
			<div class="pagination pagination-style">
				<ul>
					<li><a href="#" class="active">1</a></li>
    				<li><a href="#">2</a></li>
    				<li><a href="#">3</a></li>
    				<li><a href="#">4</a></li>
    				<li><a href="#">5</a></li>
    				<li><a href="#">6</a></li>
    				<li><a href="#">7</a></li>
    				<li><a href="#">8</a></li>
    				<li><a href="#">9</a></li>
    				<li><a href="#">10</a></li>
    				<li class="disabled"><span>...</span></li>
    				<li><a href="#">Proximo</a></li>
    				<li><a href="#">Anterior</a></li>
				</ul>
			</div>
		</div>
	</div>
@stop