@extends('admin.template')

@section('conteudo')
	
		{{ Form::open(array('url' => 'administrator/artigos/adicionar', 'enctype' => 'multipart/form-data')) }}
			<fieldset>
				@if ( count($errors) > 0)
			        <div class="alert alert-error">
			        	<a class="close" data-dismiss="alert">×</a>
			            Erros encontrados:<br />
			            <ul>
			                @foreach ($errors->all() as $e)
			                    <li>{{ $e }}</li>
			                @endforeach
			        	</ul>
			    	</div>
			    @endif
			    <legend>Adicionar Artigos
			    	<div class="btns">
			    		{{ Form::submit('Salvar') }}
			    		{{ HTML::link('administrator/artigos', 'Cancelar') }}
			    	</div>
			    </legend>
			    <p>Titulo da categoria: </p>
			    {{ Form::text('titulo') }}
			    <p>Conteudo: </p>
			    {{ Form::textarea('conteudo') }}<br />
			    <select name="categoriaId">
					<option value="">Categoria</option>
					@foreach($categoria as $categorias)
						<option value="{{ $categorias->id }}">{{$categorias->titulo}}</option>
					@endforeach
				</select>
				<p>Imagem: </p>
			    {{ Form::file('imagem', array('id'=>'imagem')) }}
	  		</fieldset>
		{{ Form::close() }}
	

@stop