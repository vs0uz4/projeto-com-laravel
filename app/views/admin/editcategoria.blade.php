@extends('admin.template')

@section('conteudo')
	{{ Form::open(array('url' => 'administrator/categorias/' . $categoria->id)) }}
		<fieldset>
			@if ( count($errors) > 0)
		        <div class="alert alert-error">
		        	<a class="close" data-dismiss="alert">×</a>
		            Erros encontrados:<br />
		            <ul>
		                @foreach ($errors->all() as $e)
		                    <li>{{ $e }}</li>
		                @endforeach
		        	</ul>
		    	</div>
		    @endif
		    <legend>Editar Categoria
		    	<div class="btns">
		    		{{ Form::submit('Salvar') }}
		    		{{ HTML::link('administrator/categorias', 'Cancelar') }}
		    	</div>
		    </legend>
		    <p>Titulo da categoria: </p>
		    {{ Form::text('titulo', $categoria->titulo) }}		    
  		</fieldset>
	{{ Form::close() }}
@stop