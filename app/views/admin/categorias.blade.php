@extends('admin.template')

@section('conteudo')
	<div class="topo-categorias">
		<legend>Categorias
	    	<div class="btns">
	    		{{ HTML::link('administrator/categorias/adicionar', 'Adicionar') }}
	    	</div>
	    </legend>
	</div>
	@if(count($categorias) > 0)
		<table class="table table-bordered table-striped table-hover">
			@if(Session::has('ok_msg'))
	    		<div class="alert alert-success"><a class="close" data-dismiss="alert">×</a>{{ Session::get('ok_msg') }}</div>
			@endif
			@if(Session::has('err_msg'))
	    		<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a>{{ Session::get('err_msg') }}</div>
			@endif
			<tr>
				<td><b>Titulo</b></td>
				<td><b>N° Artigos</b></td>
				<td><b>Editar</b></td>
				<td><b>Excluir</b></td>
			</tr>
		
			@foreach($categorias as $categoria)
				<tr>
					<td>{{ $categoria->titulo }}</td>
					<td>123</td>
					<td><a href="{{ URL::to('administrator/categorias')}}/{{$categoria->id }}">Editar</a></td>
					<td><a href="{{ URL::to('administrator/categorias/excluir')}}/{{$categoria->id }}">Excluir</a></td>
				</tr>
			@endforeach
		
		</table>
	@else	
		<div class="alert"><center><b>Nenhuma categoria cadastrada!</b></center></div>
	@endif
@stop