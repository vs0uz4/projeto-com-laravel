<!DOCTYPE html>

<html lang="pt-BR">
<head>
	<meta charset="utf-8">
	<title>Painel de Admistração - Projeto Laravel</title>
	{{ HTML::style("assets/css/admin-style.css") }}
	{{ HTML::style("assets/css/bootstrap.css") }}
	{{ HTML::style("assets/css/bootstrap-responsive.min.css") }}
	{{ HTML::script("assets/js/jquery-1.11.1.min.js", array('type' => 'text/javascript' )) }}
	{{ HTML::script("assets/js/bootstrap.js", array('type' => 'text/javascript' )) }}
	{{ HTML::script("assets/js/admin-functions.js", array('type' => 'text/javascript' )) }}
</head>

<body>
	<div id="topo">
		<div class="center-topo">
			<div class="logo">
				<h1>Painel Administrativo</h1>
			</div>
			<div class="admin">
				<p>Bem vindo <b>{{ Auth::user()->name }}</b> ao painel administrativo<br />
				{{ HTML::link('administrator/logout', 'Desconectar do painel')}}</p>
			</div>
		</div>
	</div>
	<div id="menu">
		<div class="center-menu">
			<div class="navbar navbar-editado">
				<div class="navbar-inner navbar-inner-editado">
					<ul class="nav">
						<li {{ (Request::is('administrator') ? 'class="active"' : '') }}>{{ HTML::link('administrator', 'Home')}}</li>
				      	<li {{ (Request::is('administrator/categorias')||Request::is('administrator/categorias/'.@$categoria->id)||Request::is('administrator/categorias/adicionar') ? 'class="active"' : '') }}>{{ HTML::link('administrator/categorias', 'Categorías')}}</li>
				      	<li {{ (Request::is('administrator/artigos')||Request::is('administrator/artigos/'.@$categoria->id)||Request::is('administrator/artigos/adicionar') ? 'class="active"' : '') }}>{{ HTML::link('administrator/artigos', 'Artígos')}}</li>
				    </ul>
				</div>
			</div>
		</div>
	</div>
	<div id="conteudo">
		<div class="center-conteudo">
			<div class="padding-center-conteudo">
				<div class="conteudo-box">
					@section('conteudo')
					@show
				</div>
			</div>
		</div>
	</div>
	<div id="footer">
		<div class="center-footer">
			<p>© Samuel Carvalho. Todos os direitos reservados.</p>
		</div>
	</div>
</body>
</html>