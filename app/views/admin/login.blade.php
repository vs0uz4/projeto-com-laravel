<!DOCTYPE html>

<html lang="pt-BR">
<head>
	<meta charset="utf-8">
	<title>Painel de Admistração - Projeto Laravel</title>
	{{ HTML::style("assets/css/admin-style.css") }}
	{{ HTML::style("assets/css/bootstrap.css") }}
	{{ HTML::style("assets/css/bootstrap-responsive.min.css") }}
	{{ HTML::script("assets/js/jquery-1.11.1.min.js", array('type' => 'text/javascript' )) }}
	{{ HTML::script("assets/js/bootstrap.js", array('type' => 'text/javascript' )) }}
	{{ HTML::script("assets/js/admin-functions.js", array('type' => 'text/javascript' )) }}	
</head>

<body>
	<div id="container-login">
		<div class="login">
			<div class="titulo">
				<h1>Painel de Controle</h1>
			</div>
			<div class="conteudo">
				@if ( count($errors) > 0)
	                <div class="alert alert-error">
	                	<a class="close" data-dismiss="alert">×</a>
		                Erros encontrados:<br />
		                <ul>
			                @foreach ($errors->all() as $e)
			                    <li>{{ $e }}</li>
			                @endforeach
		            	</ul>
	            	</div>
	            @endif
	            @if(Session::has('flash_notice'))
            		<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a>{{ Session::get('flash_notice') }}</div>
    			@endif

				{{ Form::open(array('url' => '/administrator')) }}
					<div class="login-container">
						{{ Form::label('email', 'E-mail: ') }}
						{{ Form::text('emailUser') }}
					</div>
					<div class="pass-container">
						{{ Form::label('pass', 'Senha: ') }}
						{{ Form::password('passUser') }}
					</div>
					<div class="submit-container">
						{{ Form::submit('Logar') }}
					</div>
				{{ Form::close() }}
			</div>
		</div>
	</div>
</body>
</html>