@extends('admin.template')

@section('conteudo')
	<div class="topo-artigos">
		<legend>Artigos
	    	<div class="btns">
	    		{{ HTML::link('administrator/artigos/adicionar', 'Adicionar') }}
	    	</div>
	    </legend>
	</div>
	@if(count($artigos) > 0)
		<table class="table table-bordered table-striped table-hover">
			@if(Session::has('ok_msg'))
	    		<div class="alert alert-success"><a class="close" data-dismiss="alert">×</a>{{ Session::get('ok_msg') }}</div>
			@endif
			@if(Session::has('err_msg'))
	    		<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a>{{ Session::get('err_msg') }}</div>
			@endif
			
			<tr>
				<td><b>Titulo</b></td>
				<td><b>Categoria</b></td>
				<td><b>Editar</b></td>
				<td><b>Excluir</b></td>
			</tr>
		
			@foreach($artigos as $artigo)
				<tr>
					<td>{{ $artigo->titulo }}</td>
					<td>{{ $artigo->categorias_id }}</td>
					<td><a href="{{ URL::to('administrator/artigos')}}/{{$artigo->id }}">Editar</a></td>
					<td><a href="{{ URL::to('administrator/artigos/excluir')}}/{{$artigo->id }}">Excluir</a></td>
				</tr>
			@endforeach
			
		</table>
	@else	
		<div class="alert"><center><b>Nenhuma notícia cadastrada!</b></center></div>
	@endif	
@stop