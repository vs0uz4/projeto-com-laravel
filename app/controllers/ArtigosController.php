<?php

class ArtigosController extends BaseController {

	public function index()
	{
		$artigos = Artigo::all();
 
        $categorias = Categoria::all();
        return View::make('admin.artigos')->with(array('artigos' => $artigos, 'categorias' => $categorias) );
	}

	public function adicionarArtigo()
	{
		$categoria = Categoria::all();
		return View::make('admin.addartigos')->with('categoria', $categoria );
	}

	public function adicionarArtigoSalvar()
	{
		$rules = array('titulo' => 'required', 'conteudo' => 'required', 'categoriaId' => 'required|integer', 'imagem' => 'image|mimes:jpeg,jpg,bmp,png,gif|max:3000');
	 
		
		$validation = Validator::make(Input::all(), $rules);
	 
		
		if ($validation->fails()) {
			return Redirect::to('administrator/artigos/adicionar')->withInput()->withErrors($validation);
		} else {
			$artigo = new Artigo;
			$artigo->titulo = Input::get('titulo');
			$artigo->conteudo = Input::get('conteudo');
			$artigo->categorias_id = Input::get('categoriaId');
            
            // Instanciando o Arquivo Enviado
			$imagefile = Input::file('imagem');
            
            // Setando o Nome Original do Arquivo Enviado
        	$filename = date('Y-m-d-H-i-s')."-".$imagefile->getClientOriginalName();
            
            // Setando o Path Absoluto de Destino de Salvamento da Imagem
            $dpath = public_path() . '/assets/img/artigos/';
            
            // Salvando o Arquivo no Disco no Path e Com o Nome Informado
			Input::file('imagem')->move($dpath, $filename);
			
            // Setando o Path Relativo da Imagem no Objeto 
            $artigo->img = 'assets/img/artigos/' . $filename;
            
            // Persistindo o Objeto Artigo no Banco de Dados.
			$artigo->save();
	 
			return Redirect::to('administrator/artigos')->with('ok_msg', 'Artigo criado com sucesso!');
		}
	}

}