<?php

class CategoriasController extends BaseController{

	public function index() // lista todas as categorias
	{
		
        $categorias = Categoria::all();
 
        
        return View::make('admin.categorias')->with('categorias', $categorias );
	}

	public function editarCategoria($id) // Procura a categoria no banco de dados atravez do email, e leva ela para o view editcategoria.blade.php
	{
		
     	$categoria = Categoria::find($id);
 
     	
        return View::make('admin.editcategoria')->with('categoria', $categoria );	
	}

	public function atualizarCategoria($id) // metodo post para atualizar a categoria de mesmo id
    {
		
		$rules = array('titulo' => 'required');
	 
		
		$validation = Validator::make(Input::all(), $rules);
	 
		
		if ($validation->fails()) {
			return Redirect::to('administrator/categorias/' . $id)->withInput()->withErrors($validation);
		} else {
			$categoria = Categoria::find($id);
			$categoria->titulo = Input::get('titulo');
			$categoria->save();
	 
			return Redirect::to('administrator/categorias');
		}
    }

    public function adicionarCategoria() // leva a view para adicionar uma nova categoria
    {
    	return View::make('admin.addcategoria');
    }

    public function adicionarCategoriaSalvar() // salva nova categoria no banco de dados
    {
    	$rules = array('titulo' => 'required|unique:categorias');
	 
		
		$validation = Validator::make(Input::all(), $rules);
	 
		
		if ($validation->fails()) {
			return Redirect::to('administrator/categorias/adicionar')->withInput()->withErrors($validation);
		} else {
			$categoria = new Categoria;
			$categoria->titulo = Input::get('titulo');
			$categoria->save();
	 
			return Redirect::to('administrator/categorias');
		}
    }

    public function excluirCategoria($id) // deleta categoria com o mesmo id
    {
    	$categoria = Categoria::find($id);
    	if($categoria->delete())
    		return Redirect::to('administrator/categorias')->with('ok_msg', 'Categoria excluida com sucesso');
    	else
    		return Redirect::to('administrator/categorias')->with('err_msg', 'Erro ao excluir categoria');
    }
}