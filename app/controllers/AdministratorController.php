<?php

class AdministratorController extends BaseController{

	public function getAdmin()
	{
		return View::make('admin.login');
	}

	public function postAdmin()
	{
		$user = array('email' => Input::get('emailUser'), 'password' => Input::get('passUser'));

		$validator = Validator::make(Input::all(), User::$rulesLogin);
		
		if( $validator->fails() ){
            return View::make('admin.login')->withErrors($validator);
        } else {
        	if (Auth::attempt($user)) {
				return View::make('admin.home');
			} else {
				return View::make('admin.login')->withErrors('Email ou senha nao válidos!');
			}
		}
	}

	public function logoutAdmin()
	{
		Auth::logout();
		return Redirect::to('administrator');
	}
}